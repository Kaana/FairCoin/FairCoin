faircoin Core integration/staging repository
=====================================

[![Build Status](https://travis-ci.org/faircoin-Project/faircoin.svg?branch=master)](https://travis-ci.org/faircoin-Project/faircoin) [![GitHub version](https://badge.fury.io/gh/faircoin-Project%2Ffaircoin.svg)](https://badge.fury.io/gh/faircoin-Project%2Ffaircoin)

faircoin is a cutting edge cryptocurrency, with many features not available in most other cryptocurrencies.
- Anonymized transactions using coin mixing technology, we call it _Coin Mixing_.
- Fast transactions featuring guaranteed zero confirmation transactions, we call it _FastSend_.
- Decentralized blockchain voting providing for consensus based advancement of the current Masternode
  technology used to secure the network and provide the above features, each Masternode is secured
  with collateral of 25K faircoin

More information at [faircoin.io](http://www.faircoin.io)

### Coin Specs
<table>
<tr><td>Ticker Symbol</td><td>FAIR</td></tr>
<tr><td>Algorithm</td><td>Quark</td></tr>
<tr><td>Type</td><td>PoW - PoS Hybrid*</td></tr>
<tr><td>Block Time</td><td>120 Seconds</td></tr>
<tr><td>Difficulty Retargeting</td><td>Every Block</td></tr>
<tr><td>Max Coin Supply</td><td>53,193,831 FAIR</td></tr>
<tr><td>Premine/Initial Supply</td><td>90%</td></tr>
<tr><td>faircoin Created Per block</td><td>10 FAIR*</td></tr>
</table>


### PoS/PoW Block Details
<table>
<tr><td>Proof Of Work Phase</td><td>1-25000 Blocks.</td></tr>
<tr><td>Proof of Stake Phase</td><td>25001-infinite</td></tr>
</table>

### Staking Methods
<table>
<tr><td>Masternodes</td><td>Yes - 25k faircoin Required</td></tr>
<tr><td>Wallet Staking</td><td>Yes - 1 to infinite faircoin</td></tr>
</table>

</table>
